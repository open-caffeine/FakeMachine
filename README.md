# FakeMachine

Jura coffee machine emulator.

## Schaltplan
| BlueFrog | Arduino Mega |
|----------|--------------|
| +5v      | pin 22       |
| TX       | RX3          |
| GND      | GND          |
| RX       | TX3          |

Das Modul an Pin 22 anzuschließen ist okay, es braucht nur 10mA.

## Bedienung
Da eh ein Parser für Jura-Nachrichten da war, lag es nahe den zu benutzen

| Befehl | Bedeutung |
|--------|-----------|
| `po:<1/0>` | Power: schaltet das Bluetooth-Modul an (`po:1`) oder aus|
| `mp:<wert>` | Setze die drei byte des Maintenance Percent zählers |
| `mc:<wert>` | Setze die 12 byte des Maintenance Counter |
| `fv:<1/0>` | Schalte  Ausgabe beim periodischen Versenden von Flags an/aus |
| `fs:<bit>` | Setze bit in der Statusmaske |
| `fc:<bit>` | Lösche bit in der Statusmaske |
| `:,<text>` | Versende `<text>` |
| `@:,<text>` | Versende `<text>` mit obfuscation |
| `mem` | Heap-größe und freien speicher ausgeben |

