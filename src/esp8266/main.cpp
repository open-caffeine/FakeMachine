#include <SoftwareSerial.h>
#include "main.h"

SoftwareSerial serial(D1, D2);
JuraSerial<SoftwareSerial> toFrogImpl(serial);
IJuraSerial& toFrog(toFrogImpl);

bool handlePlatformCommand(const JuraMessageView&)
{
	return false;
}

