/**
 * Copyright (c) 2021-2022 Alexander Fuchs <alex.fu27@gmail.com> and the
 * contributors of the open-caffeine project.
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 *
 * We are not affiliated, associated, authorized, endorsed by, or in any way
 * officially connected with Jura Elektroapparate AG. JURA and the JURA logo are
 * trademarks or registered trademarks of Jura Elektroapparate AG in Switzerland
 * and/or other countries.
 *
 * Using our software or hardware with you coffee machine may void your warranty
 * and we cannot be held liable for any damage or operating failure.
 */
#include <Arduino.h>
#include <JuraInterface.h>
#include <JuraObfuscation.h>
#include <JuraSerial.h>
#include "FakeMachine.h"
#include "main.h"

JuraObfuscator obfuscator(Permutation::P1, Permutation::P2);
JuraInterface interface(toFrog, obfuscator);

FakeMachine fm(interface, obfuscator, Serial);

StringBuffer commandBuffer(128);

bool updateSerial()
{
	while (Serial.available()) {
		char c = Serial.read();
		if (c == '\b') {
			commandBuffer.pop();
			Serial.write("\b \b");
			continue;
		}

		commandBuffer.put(c);
		Serial.write(c);
		if (commandBuffer.isTerminated()) {
			return true;
		}
	}
	return false;
}

void handleCommand(const JuraMessageView& cmd)
{

	if (strcmp(cmd.getCommand(), "mp") == 0) { // maintenance percent
		fm.setMaintenancePercent(cmd.getFirstParameter());
		return;
	}

	if (strcmp(cmd.getCommand(), "mc") == 0) { // maintenance counter
		fm.setMaintenanceCounter(cmd.getFirstParameter());
		return;
	}

	if (strcmp(cmd.getCommand(), "fv") == 0) { // flags visible
		char is = cmd.getFirstParameter()[0];
		fm.setFlagsVerbosity((FakeMachine::SendFlagsVerbosity) (is - '0'));
		return;
	}

	if (strcmp(cmd.getCommand(), "fs") == 0) { // flag set
		int num = atoi(cmd.getFirstParameter());
		fm.setFlag(num, true);
		return;
	}

	if (strcmp(cmd.getCommand(), "fc") == 0) { // flag clear
		int num = atoi(cmd.getFirstParameter());
		fm.setFlag(num, false);
		return;
	}

	if (strcmp(cmd.getCommand(), "gcc") == 0) { // get current coffee
		const Product& caff = fm.getCurrentProduct();
		StringBuffer sb(64);
		caff.format(sb);
		Serial.write(sb.get());
		return;
	}

	if (strcmp(cmd.getCommand(), "s") == 0) { // send
		const char* par = cmd.getSecondParameter();
		fm.send(par, false);
		return;
	}

	if (strcmp(cmd.getCommand(), "") == 0) { // direct send
		const char* par = cmd.getSecondParameter();
		if (cmd.isNewProtocol()) {
			interface.sendObfuscated(par);
		} else {
			interface.sendPlain(par);
		}
		return;
	}

	if (!handlePlatformCommand(cmd)) {
		Serial.write(cmd.getCommand());
		Serial.write(" ???\r\n");
	}
}

void updateCommands()
{
	if (!updateSerial()) {
		return;
	}

	JuraMessageView view(commandBuffer.get());

	handleCommand(view);

	commandBuffer.clear();
}

void setup()
{
	Serial.begin(115200);
	toFrog.begin();
	pinMode(22, OUTPUT);
	while (!Serial);
	digitalWrite(22, true);
}

void loop()
{
	interface.update();
	fm.update();
	updateCommands();
}
