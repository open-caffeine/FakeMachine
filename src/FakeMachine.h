#include <JuraInterface.h>
#include <Product.h>
#include <Progress.h>

class JuraInterface;
class JuraObfuscator;
class HardwareSerial;

class FakeMachine
{
public:
	enum class SendFlagsVerbosity {
		ALL = 0,
		PROGRESS_ONLY = 1,
		NONE = 2
	};

private:
	JuraInterface& interface;
	JuraObfuscator& obfuscator;
	HardwareSerial& debugSerial;

	const char* model = "EF567M V01.18";
	uint16_t modelNo = 0x36BB;
	const char* register37 = "BCD8000536783591";
	uint8_t flags[5];
	StringBuffer maintenancePercent;
	StringBuffer maintenanceCounter;

	enum class AutoStep {
		NONE,
		HANDSHAKE1,
		HANDSHAKE2
	};

	AutoStep nextStep;

	bool obfuscationOn = false;

	enum class SendFlags {
		OFF,
		FLAGS,
		PROGRESS
	};
	SendFlags doSendFlags = SendFlags::OFF;

	SendFlagsVerbosity sendFlagsVerbosity = SendFlagsVerbosity::PROGRESS_ONLY;

	unsigned long sendFlagsNext = 0;

	Product currentProduct;
	ProgressBuffer currentProgress;

	enum class BrewState {
		OFF,
		GRINDING,
		BREWING,
		ENJOY
	};
	BrewState brewState = BrewState::OFF;
	int brewStateExtra = 0;
	unsigned long brewUpdateNext = 0;

	void sendFlags();
	void sendProgress();
	void handleOldProtocol(const JuraMessageView& msg);
	void handleSoleCommand(const JuraMessageView& msg);
	void handle1PCommand(const JuraMessageView& msg);
	void handle2PCommand(const JuraMessageView& msg);
	void handleNewProtocol(const JuraMessageView& msg);
	void handleMessage(const JuraMessageView& msg);

public:
	FakeMachine(JuraInterface& interface, JuraObfuscator& obfuscator,
			HardwareSerial& debugSerial);

	void setMaintenancePercent(const char* val);
	void setMaintenanceCounter(const char* val);
	void setFlagsVerbosity(SendFlagsVerbosity);
	void setFlag(uint8_t index, bool value);

	void startBrewing(const Product& c);
	void updateBrewing();
	void stopBrewing();

	const Product& getCurrentProduct() const;
	const ProgressBuffer& getCurrentProgress() const;

	void send(const char* message, bool silent=false);
	void send(const StringBuffer& message, bool silent=false);

	void update();
};

